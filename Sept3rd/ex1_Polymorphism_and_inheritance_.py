#Polymorphism is the capability of a method to be applied to different type of objects
#so the method has its on version of itself, deppending of the object it's beeing applied to

#Inheritance in Python allows us to determine attributes to a parent class, and all subclass
#that belongs to the parent class, will have those attributes by default. In other words:
#The attributes of a parent class won't be needed to be specified in any subclass that extends it

class ExoticCars:
    numberOfWheels= 2
    numberOfDoors = 2
    def raceAQuarterMile(self):
        return 0

    def topSpeed(self):
        return 0

class Lamborghini(ExoticCars):
    def raceAQuarterMile(self):
        print("A Lambo finishes a quarter mile at 10.4 seconds (AVG)")

    def topSpeed(self):
        print("A Lambo Top Speed is: 350 kph")

class Koenigsegg(ExoticCars):
    def raceAQuarterMile(self):
        print("A Koenigsegg finishes a quarter mile at 10.0 seconds (AVG)")

    def topSpeed(self):
        print("A Koenigsegg Top Speed is: 437 kph")

aventador = Lamborghini()
aventador.raceAQuarterMile()
aventador.topSpeed()
print("Aventador's number of doors: ", aventador.numberOfDoors)
print("Aventador's number of wheels: ", aventador.numberOfWheels)


agera = Koenigsegg()
agera.raceAQuarterMile()
agera.topSpeed()
print("Koenigsegg's number of doors: ", aventador.numberOfDoors)
print("Koenigsegg's number of wheels: ", aventador.numberOfWheels)
