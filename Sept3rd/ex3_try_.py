#In python some cases and methods may fail and fall into exceptions.
#"Try" and "except" allow us to handle the possible exceptions in order to:
#-make our code friendly for the user
#-specify what kind of exception we are getting and how to solve it
#-keep the code running and trying to take alternate ways

#For instance: this piece of code allows us to ask for a integer number
#If the user types a double, a letter or a 0, it will tell print out what is wrong with the input
#in stead of throwing a console error the user won't be able to understand

try:
    input = int(input("Please type a integer number to calculate it's inverse: "))
    inverse = 1 / input
    print("The inverse of", input, "is: ", inverse)


except (ZeroDivisionError, TypeError, ValueError, IndexError):
    print("The number you entered is not valid or is not a number.")
    print("Remember division by 0 is not possible.")



